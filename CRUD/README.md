# CRUD 
## node.js express.js postgresql 
------
## GET
get all users
```
curl --get http://localhost:6000/users
```
get user based on id
```
curl --get http://localhost:6000/users/#
```

------

## POST
```
curl --data "name=thirduser&email=thurdy@thirdy.com" http://localhost:6000/users
```
------

## PUT
Change user with id # 2
```
curl -X PUT -d "name=samaras" -d "email=aditya@samaras.com"  http://localhost:6000/users/2
```

------

## DELETE
delete user with id 3
```
curl -X "DELETE" http://localhost:6000/users/3
```

------

